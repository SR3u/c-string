//
//  main.c
//  c-string
//
//  Created by Sergey Rump on 27.04.15.
//  Copyright (c) 2015 SR3u. All rights reserved.
//

#include <stdio.h>
#include "c-string/c-string.h"
STRING_PROCESSOR(test_char_processor)
{
    *chr='*';
    return 1;
}
void test_algorythms(void)
{
    string str=string_create("HeLlO wOrLd!");
    printf("string str=string_create(\"HeLlO wOrLd!\");\n");
    string_tolowercase(str);
    printf("string_tolowercase(str);\nstr: '%s'\n",str.str);
    printf("string_destroy(&str);\n");
    string_destroy(&str);
    str=string_create("foo\nbar\nfizz\nbuzz");
    printf("str=string_create(\"foo\\nbar\\nfizz\\nbuzz\");\n");
    string_process(&str,cutbyendl);
    printf("string_process(&str,cutbyendl);\nstr: '%s'",str.str);
    printf("string_destroy(&str);\n");
    string_destroy(&str);
    str=string_create("foo bar fizz buzz");
    printf("str=string_create(\"foo bar fizz buzz\");\n");
    string_process(&str,cutbyspace);
    printf("string_process(&str,cutbyspace);\nstr: '%s'\n",str.str);
    printf("string_destroy(&str);\n");
    string_destroy(&str);
    str=string_create("foo  bar  fizz  buzz");
    printf("str=string_create(\"foo  bar  fizz  buzz\");\n");
    string_process(&str,spacestotabs);
    printf("string_process(&str,spacestotabs);\nstr: '%s'\n",str.str);
    string_process(&str,tabstospaces);
    printf("string_process(&str,tabstospaces);\nstr: '%s'\n",str.str);
}
void test_core(void)
{
    string str=string_create("Hello");
    printf("string_create(\"Hello\");\nstr: '%s'\n",str.str);
    string str2=string_create(" World!");
    printf("string str2=string_create(\" World!\");\n");
    printf("str2: '%s'",str2.str);
    string str3=string_add(str, str2);
    printf("string str3=string_add(str, str2);\nstr3: ");
    printf("'%s'\n",str3.str);
    printf("string_char(str2,str2.len-2) '%c'\n",string_char(str2,str2.len-2));
    printf("string_firstchar(str) '%c'\n",string_firstchar(str));
    printf("string_lastchar(str2) '%c'\n",string_lastchar(str2));
    string str4=string_copy(str3);
    printf("string str4=string_copy(str3);\n");
    printf("str4.str[0]=string_lastchar(str3);\n");
    str4.str[0]=string_lastchar(str3);
    printf("str4: '%s'\nstr3: '%s'\n",str4.str,str3.str);
    string_destroy(&str);
    printf("string_destroy(&str);\nstr: '%s'\n",str.str);
    string_removecharatindex(&str2, 4);
    printf("string_removecharatindex(&str2, 4);\nstr2: '%s'\n",str2.str);
    string_insertcharatindex(&str2,'*',4);
    printf("string_insertcharatindex(&str2,'*',4);\nstr2: '%s'\n",str2.str);
    string_process(&str4, test_char_processor);
    printf("STRING_PROCESSOR(test_char_processor)\n"
           "{\n\t*chr='*';\n\treturn 1;\n}\n");
    printf("string_process(&str4, test_char_processor);\n");
    printf("str4: '%s'\n",str4.str);
}
int main(int argc, const char * argv[])
{
    test_algorythms();
    //test_core();
    printf("end\n");
    return 0;
}
