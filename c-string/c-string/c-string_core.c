//
//  c-string_core.c
//  c-string
//
//  Created by Sergey Rump on 28.04.15.
//  Copyright (c) 2015 SR3u. All rights reserved.
//

#include "c-string_core.h"

#include <string.h>
#include <stdlib.h>

string string_create(character_t* c_str)
{
    string str;
    str.len=strlen(c_str);
    str.str=malloc(strsize(str.len));
    strcpy(str.str, c_str);
    return str;
}
string string_copy(string str)
{
    return string_create(str.str);
}
void string_destroy(string* str)
{
    if(str->str!=NULL)
        free(str->str);
    str->str=NULL;
    str->len=0;
}
character_t string_char(string str,strlen_t pos)
{
    if(str.len>=pos)
        return str.str[pos];
    return '\0';
}
character_t string_firstchar(string str)
{return string_char(str, 0);}
character_t string_lastchar(string str)
{return string_char(str,str.len-1);}
string string_add(string str1,string str2)
{
    string res;
    res.len=str1.len+str2.len;
    res.str=malloc(strsize(res.len));
    strcpy(res.str,str1.str);
    strcat(res.str, str2.str);
    return res;
}
strlen_t string_length(string *str)
{
    str->len=strlen(str->str);
    return str->len;
}
void string_insertcharatindex(string *str,character_t chr,strlen_t idx)
{
    character_t *cstr=str->str;
    str->str=malloc(strsize(str->len+1));
    memcpy(str->str, cstr,strsize(str->len));
    memmove(&str->str[idx],&chr,sizeof(character_t));
    memmove(&str->str[idx+1],&cstr[idx],strsize(str->len-idx));
    free(cstr);
    str->len+=1;
}
void string_removecharatindex(string *str,strlen_t idx)
{
    memmove(&str->str[idx], &str->str[idx+1],str->len-idx);
    //memmove(word[idxToDel], &word[idxToDel + 1], strlen(word) - idxToDel);
}
int string_process(string *str, char_processor_t processor)
{
    if(str->str==NULL){return 1;}
    int res=0;
    for (strlen_t i=0; str->str[i]!=0; i++)
    {
        res=processor(&(str->str[i]),i,str);
        if(res<0){return res;}
    }
    return 0;
}