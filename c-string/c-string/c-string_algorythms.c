//
//  c-string_algorythms.c
//  c-string
//
//  Created by Sergey Rump on 28.04.15.
//  Copyright (c) 2015 SR3u. All rights reserved.
//

#include "c-string_algorythms.h"
#include <string.h>
#include <stdlib.h>
STRING_PROCESSOR(tolowercase)
{
    *chr=tolower(*chr);
    return 1;
}
STRING_PROCESSOR(touppercase)
{
    *chr=toupper(*chr);
    return 1;
}
STRING_PROCESSOR(cutbyendl)
{
    if(*chr=='\n')
    {
        *chr=0;
        character_t *cstr=str->str;
        *str=string_create(cstr);
        free(cstr);
        return 0;
    }
    return 1;
}
STRING_PROCESSOR(cutbyspace)
{
    if(*chr==' ')
    {
        *chr=0;
        character_t *cstr=str->str;
        *str=string_create(cstr);
        free(cstr);
        return 0;
    }
    return 1;
}
STRING_PROCESSOR(replacebystar)
{
    *chr='*';
    return 1;
}
STRING_PROCESSOR(replacebyspace)
{
    *chr=' ';
    return 1;
}
STRING_PROCESSOR(replacebydot)
{
    *chr='.';
    return 1;
}
STRING_PROCESSOR(spacestotabs)
{
    static character_t *prev_chr;
    if(prev_chr!=NULL)
        if(*prev_chr==' '&&*chr==' ')
        {
            *prev_chr='\t';
            string_removecharatindex(str,pos);
        }
    prev_chr=chr;
    return 1;
}
STRING_PROCESSOR(tabstospaces)
{
    if(*chr=='\t')
    {
        *chr=' ';
        string_insertcharatindex(str, ' ', pos);
    }
    return 1;
}

void string_tolowercase(string str)
{
    string_process(&str, tolowercase);
}
void string_touppercase(string str)
{
    string_process(&str, touppercase);
}