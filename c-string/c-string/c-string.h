//
//  c-string.h
//  c-string
//
//  Created by Sergey Rump on 27.04.15.
//  Copyright (c) 2015 SR3u. All rights reserved.
//

#ifndef __c_string__c_string__
#define __c_string__c_string__

#include "c-string_core.h"
#include "c-string_algorythms.h"

#endif /* defined(__c_string__c_string__) */
