//
//  c-string_core.h
//  c-string
//
//  Created by Sergey Rump on 28.04.15.
//  Copyright (c) 2015 SR3u. All rights reserved.
//

#ifndef __c_string__c_string_core__
#define __c_string__c_string_core__

typedef unsigned long long strlen_t;

typedef char character_t;
#define strsize(len) len*sizeof(character_t)

typedef struct
{
    strlen_t len;
    character_t* str;
}string;

string string_create(character_t* c_str);// Create string from 0-terminated string
string string_copy(string str);// Create string from another string
void string_destroy(string *str);// deallocate memory
string string_add(string str1,string str2);// concatenation
strlen_t string_length(string *str);// recalculate length of string (not really needed)
char string_char(string str,strlen_t pos);// get character from string on position 'pos'
char string_firstchar(string str);// get first character
char string_lastchar(string str);// get last character
void string_removecharatindex(string *str,strlen_t idx);//removes character at index idx
void string_insertcharatindex(string *str,character_t chr,strlen_t idx);//inserts character at index idx

typedef int (*char_processor_t)(character_t* chr,strlen_t pos,string *str);
#define STRING_PROCESSOR(name) int name(character_t* chr,strlen_t pos,string *str)
int string_process(string *str, char_processor_t processor);//processes string via char_processor function
                                                            //if processor return value > 0 it gives next character
                                                            //and interrupts it otherwise. returns 1 if error 

#endif /* defined(__c_string__c_string_core__) */
