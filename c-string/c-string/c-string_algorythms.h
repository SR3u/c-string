//
//  c-string_algorythms.h
//  c-string
//
//  Created by Sergey Rump on 28.04.15.
//  Copyright (c) 2015 SR3u. All rights reserved.
//

#ifndef __c_string__c_string_algorythms__
#define __c_string__c_string_algorythms__

#include "c-string_core.h"
#include <ctype.h>

STRING_PROCESSOR(tolowercase);
STRING_PROCESSOR(touppercase);
STRING_PROCESSOR(cutbyendl);
STRING_PROCESSOR(cutbyspace);
STRING_PROCESSOR(replacebystar);
STRING_PROCESSOR(replacebyspace);
STRING_PROCESSOR(replacebydot);
STRING_PROCESSOR(spacestotabs);
STRING_PROCESSOR(tabstospaces);

void string_tolowercase(string str);
void string_touppercase(string str);

#endif /* defined(__c_string__c_string_algorythms__) */
